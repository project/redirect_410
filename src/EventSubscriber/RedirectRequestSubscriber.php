<?php

namespace Drupal\redirect_410\EventSubscriber;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\redirect\RedirectChecker;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Redirect subscriber for controller requests.
 */
class RedirectRequestSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\redirect\RedirectChecker
   */
  protected $checker;

  /**
   * A path processor manager for resolving the system path.
   *
   * @var \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  protected $pathProcessor;

  /**
   * Constructs a Drupal\redirect_410\EventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\redirect\RedirectChecker $checker
   *   The redirect checker service.
   * @param \Drupal\Core\PathProcessor\InboundPathProcessorInterface $path_processor
   *    The path processor service.
   */
  public function __construct( EntityTypeManagerInterface $entity_type_manager, RedirectChecker $checker, InboundPathProcessorInterface $path_processor) {
    $this->entityTypeManager = $entity_type_manager;
    $this->checker = $checker;
    $this->pathProcessor = $path_processor;
  }

  /**
   * Handles the redirect if any found.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   *
   */
  public function onKernelRequestCheckRedirect(RequestEvent $event) {
    $request = clone $event->getRequest();

    if (!$this->checker->canRedirect($request)) {
      return;
    }

    if (strpos($request->getPathInfo(), '/system/files/') === 0 && !$request->query->has('file')) {
      $path = $request->getPathInfo();
    }
    else {
      $path = $this->pathProcessor->processInbound($request->getPathInfo(), $request);
    }

    try {
      $storageHttpStatusEntity = $this->entityTypeManager->getStorage('http_status_entity');
    } catch (InvalidPluginDefinitionException $e) {
      return;
    } catch (PluginNotFoundException $e) {
      return;
    }

    $entities = $storageHttpStatusEntity->loadByProperties(['url' => $path]);
    if (!$httpStatus = reset($entities)) {
      return;
    }
    $statusCode = $httpStatus->status_code;
    if($statusCode !== '410'){
      return;
    }

    $content = t('<h1>Gone</h1><p>The requested resource is no longer available on this server and there is no forwarding address.Please remove all references to this resource.</p>');
    $response = new CacheableResponse($content, $statusCode);
    $response->addCacheableDependency($httpStatus);
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestCheckRedirect', 34];
    return $events;
  }
}
